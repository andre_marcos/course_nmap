#reverse dns scan on the target, to list the target names
nmap facebook.com/24 -sL 

#skips the port scan after the discovery scan
nmap 192.168.1.1/24 -sn

#skips the discovery scan and goes straight to the port scan
nmap 192.168.1.1-5 -Pn

# sends a SYN packet in the discovery stage to the specified ports
nmap 192.168.1.1-5 -PS22-25,80,113,1050,35000

# sends a ACK packet in the discovery stage to the specified ports
nmap 192.168.1.1-5 -PA22-25,80,113,1050,35000

# sends a UDP datagram in the discovery stage to the specified ports
nmap -PU53 192.168.1.1-5
# sends a UDP datagram to the port 53 to 10 random host and skips the port scan
nmap -iR 10 -PU53 -sn -vv

#Scan host using an ICMP echo
nmap -PE -sn -vv 192.168.1.1-5

#ARP ping scan , only works on local network
nmap -PR -sn -vv 192.168.1.1-5

# Reverse DNS lookup
nmap 192.168.1.1-50 -sL --dns-server 192.168.1.1