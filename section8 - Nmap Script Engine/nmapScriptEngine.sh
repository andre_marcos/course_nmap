#run a script

nmap -sC <scriptName/scriptCategory/pathToScript> <Host>


#the -Pn and -n option scan speed up the exectuion of a script
nmap -n -Pn -p 80 --open -sV ---vvv --script banner,http-title -iR 100

#you can also use wildcards
nmap -v --script “http-*” 192.168.1.1

#help section of a script
nmap --script-help nameOfScript

#banner script
nmap -vvv --script=banner 192.168.1.1

#cross-site vuln script
nmap -Pn --script http-xssed scanme.nmap.org

#site map generator script
nmap --script=http-sitemap-generator scanme.nmap.org

#Find webservers
nmap -n -Pn -p 80 --open -sV ---vvv --script banner,http-title  <host>

#Brute force subdomain
nmap -Pn --script=dns-brue facebook.com

#SMB discovery host
nmap --script smb-os-discovery -vv 192.168.1.81

#broadcast script
nmap --script broadcast -v

#All not intrusive scripts
nmap --script “not intrusive” -vv 192.168.1.50

#Scripts to look for vuln and expl
nmap --script “vuln,exploit” -vv 192.168.1.50 -sV -O

#IP Geolocation
nmap --script ip-geolocation-* facebook.com

#WhoIs scripts
nmap --script whois* facebook.com

#Check if Proxy is running on port
nmap --script http-open-proxy -p8080 192.168.1.1-100

#Detect if service behind firewall
nmap -p80 --script http-waf-detect scanme.nmap.org
