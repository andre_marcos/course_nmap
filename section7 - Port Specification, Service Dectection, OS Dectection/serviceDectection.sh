#Most Basic Service Detection
nmap -sV 192.168.0.34

#Version Intensity
nmap -sV --version-intensity 1 192.168.0.34

#Debug Service Version Detection
nmap -sV 192.168.1.43 -sV --version-trace