#Specify port to scan

nmap -p 34 192.168.0.34

#Specify a range of Ports

nmap -p 1-100 192.168.0.34

#Specify Protocol in port Specification

nmap -p U:53,5353,T:21-25,80,8080 -sU -sS 192.168.0.34

#Scan all Ports
nmap -p- 192.168.0.34

#Specify Protocols to scan their ports
nmap -p http, https, ftp 192.168.0.34

#Specify Fast Mode
nmap -F 192.168.0.34

#From the first to the Specified Port
nmap -p-65489 192.168.0.34

#From the specified to the last Port
nmap -p0- 192.168.0.34

#Exclude Ports From the Scan
nmap --exclude-ports