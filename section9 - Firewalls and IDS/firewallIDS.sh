#speed up the scan
nmap -T5 192.168.1.1

#use fragmented ip packets
nmap -f 192.168.1.35

#specify ip packets size
nmap --mtu 16 192.168.1.35

#decoys
nmap -n -D 192.168.1.101,192.168.1.102,192.168.1.103,192.168.1.23 192.168.1.1

#spoof Source Address
nmap -S www.microsoft.com www.facebook.com -e eth0 -Pn

#Specify Source Port
nmap -g 53 192.168.1.1

#Proxies
nmap --proxies http://192.168.1.1:8080, http://192.168.1.2:8080 192.168.1.50


#Masking the Nmap Packets by adding payload
nmap --data-length 200 192.168.1.1
nmap --data-string “This string is going in the packet” 192.168.1.1

#relative stealthy scan
nmap -f -T 0 -n -Pn --data-length 200 -D 192.168.1.101,192.168.1.102,192.168.1.103,192.168.1.23 192.168.1.1


