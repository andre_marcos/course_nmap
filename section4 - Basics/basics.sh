# show the help manual for the nmap command
nmap -h

#default scan. Scans 1000 tcp ports (the most common)
nmap <TARGET_IP>

#scans a range of ips. In this case from 1 to 45
nmap 192.168.1.1-45

#get the targets from a file
nmap -iL <FILE_PATH>

#scans n targets at random
nmap -iR <numberOfTargets>

